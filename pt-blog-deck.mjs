import { define } from 'https://unpkg.com/synergy@7.0.4'

const tagName = 'prte-blog-card'
function factory() {
  return {
    $pieceTitle: '',
    $creatorName: '',
    $pieceSlug: '',
    $pieceDescription: '',
    $imageUri: ''
    // TODO: Add thumbnail_picture URI
  }
}
const style = /*css*/`
.blogdeck-container {
  list-style: none;
}
.blogdeck-container > .card {
  background-color: white;
  border-top-style: dashed;
  border-bottom-style: solid;
  border-right-style: dashed;
  border-left-style: solid;
  border-color: black;
  border-width: 1px;
}
`
let template = document.createElement('template')
template.innerHTML = `
<style>${style}</style>
<dl class="blogdeck-container">
  <div class="card">
    <dt>{{ $pieceTitle }}</dt>
    <dd>
      {{ $pieceDescription }}
    </dd>
  </div>
</dl>
`
/* let styleElement = document.createElement('style')
let deck = document.createElement('ol')
let card = document.createElement('li')
styleElement.append(
  `.blogdeck-container { color: red; }`
)
template.append(styleElement, deck)
deck.append(card) */

define(tagName, factory, template, {shadow: 'open'})