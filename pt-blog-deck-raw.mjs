// pt-blog-deck-raw.mjs

/* const deckTemplate = document.createElement("template")
deckTemplate.innerHTML = `
  <dl>
    <slot/>
  <dl>
` */

const cardTemplate = document.createElement("template")
cardTemplate.innerHTML = `
  <dt>
    <slot name="piece-title">
      Concise, Descriptive Slotted Headline
    </slot>
  </dt>
  <dd>
    <slot name="piece-description">
      <p>A nice, pithy description hidden in the slot.</p>
    </slot>
  </dd>
`

class PrTeCard extends HTMLDivElement {
  constructor() {
    super()
  }
  connectedCallback() {
    let cardContent = cardTemplate.content.cloneNode(true)
    let shadowRoot = this.attachShadow({ mode: "open" })
    this.append(shadowRoot); shadowRoot.append(cardContent)
  }
}

customElements.define("prte-card", PrTeCard, { extends: "div" })